package MyFirstProject;

import java.util.Scanner;

public class MyMiniDVD {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		//创建4个数组来保存DVD名称，状态，借出日期，借出次数
		String[] name = new String[6];
		int[] state = new int[6];//状态有两种：1 表示已经借出   0 表示可以借阅
		int[] date = new int[6];
		int[] count = new int[6];
		
		//预存3张DVD
		name[0] = "罗马假日";
		state[0] = 1;
		date[0] = 1;
		count[0] = 15;
		
		name[1] = "风声鹤唳";
		state[1] = 0;
		count[1] = 12;
		
		name[2] = "浪漫满屋";
		state[2] = 0;
		count[2] = 30;
		
		//定义一个全局变量来标识是否想退出程序
		//这个变量一定要定义在do-while外面，否则会报错。
		boolean flag = false;
		int num = -1;
		//系统的整个架构：用do-while来控制用户循环输入
		do{
			//输出系统的界面提示
			System.out.println("欢迎使用迷你DVD管理器");
			System.out.println("-------------------------------");
			System.out.println("1.新增DVD");
			System.out.println("2.查看DVD");
			System.out.println("3.删除DVD");
			System.out.println("4.借出DVD");
			System.out.println("5.归还DVD");
			System.out.println("6.退出DVD");
			System.out.println("-------------------------------");
			System.out.println("请输入你的选择：");
			int choose = input.nextInt();
			
			switch(choose)
			{
			//1.新增DVD
			case 1:
				System.out.println("请输入你要新增的DVD名称：");
				String newName = input.next();
				for(int i = 0; i < name.length; i++)
				{
					if(newName.equals(name[i]))
					{
						System.out.println("货架上已经有该DVD，无法新增！");
						break;
					}
					else if(name[i] == null){
						name[i] = newName;
						state[i] = 0;
						count[i] = 0; 
						System.out.println("新增DVD成功！");
						break;
					}
					//如果架子为满则提示用户
					else if(i == 5){
						System.out.println("架子上面DVD已满，无法新增DVD！");
						break;
					}
				}
				break;
			//2.查看DVD
			case 2:
				System.out.println("序号"+"\t"+"状态"+"\t\t"+"名称"+"\t\t\t"+"借出日期"+"\t"+"借出次数");
				for(int i = 0; i < name.length; i++)
				{
					if(name[i] != null)
					{
						System.out.println((i+1)+"\t"+(state[i] == 0 ? "可借出":"已借出")+"\t\t"+name[i]+"\t\t\t"
								           +(date[i] == 0 ? " ":date[i]+"号")+"\t"+count[i]+"次");
					}else{
						break;
					}
				}
				break;
			//3.删除DVD
			case 3:
				System.out.println("请输入你要删除的DVD名称：");
				String delName = input.next();
				boolean delFlag = false;
				int index = -1;//用来记录要插入的位置
				for(int i = 0; i < name.length; i++)
				{
					//当要删除的DVD名称一样并且该DVD没有被借出去
					if(delName.equals(name[i]) && state[i] == 0)
					{
						delFlag = true;
						index = i;
						break;
					}
					else if(delName.equals(name[i]) && state[i] == 1)
					{
						delFlag = true;
						System.out.println("你所要删除的DVD已借出，无法删除！");
						break;
					}
				}
				
				if(index != -1){		
					for(int j = index; j < name.length; j++)
					{
						//从后往前覆盖，一定要记得DVD名称，状态，借阅日期和借阅次数都要从后往前覆盖
						if(j != name.length - 1)
						{
							name[j] = name[j+1];
							state[j] = state[j+1];
							date[j] = date[j+1];
							count[j] = count[j+1];
						}
						else{						
								//最后一个数据不管是否为空，都把最后一个数据清空
								name[j] = null;
								state[j] = 0;
								date[j] = 0;
								count[j] = 0;
						}
					}
					System.out.println("删除成功！");
					break;
				}
				if(!delFlag)
				{
					System.out.println("没有你所选的DVD！");
					break;
				}
				break;
			//4.借出DVD
			case 4:
				System.out.println("请输入你要借的DVD名称：");
				String wanName = input.next();
				boolean wanFlag = false;
				for(int i = 0; i < name.length; i++)
				{
					if(wanName.equals(name[i]) && state[i] == 0)
					{
						wanFlag = true;
						System.out.print("请输入借阅日期：");
						int newDate = input.nextInt();
						//要考虑程序的健壮性
						while(newDate < 1 || newDate > 30){
							System.out.println("输入错误，请输入1-31之间的日期：");
							newDate = input.nextInt();
						}
						date[i] = newDate;
						state[i] = 1;//借出之后状态要记得改变成1
						count[i]++;//次数记得增加1
						System.out.println("成功借阅"+wanName);
						break;
					}
					else if(wanName.equals(name[i]) && state[i] == 1)
					{
						wanFlag = true;
						System.out.println("你所要借的"+wanName+"已借出，无法借阅！");
						break;
					}
				}
				if(!wanFlag)
				{
					System.out.println("没有你所选的DVD！");
					break;
				}
				break;
			//5.归还DVD
			case 5:
				System.out.println("请输入你要归还的DVD名称：");
				String retName = input.next();
				boolean retFlag = false;
				for(int i = 0; i < name.length; i++)
				{
					if(retName.equals(name[i]) && state[i] == 1)
					{
						retFlag = true;
						System.out.println("请输入归还的日期：");
						int retDay = input.nextInt();
						int rent;
						//这里没有考虑到要用retDay跟date[i]进行比较，还用retDay跟1来比较就是错的。
						while(retDay < date[i] || retDay > 31)
						{
							System.out.println("归还日期输入错误，必须大于借阅日期且小于31，请重新输入:");
							retDay = input.nextInt();
						}
						//向用户说明租金情况，当用户确认之后则开始计算租金
						System.out.println("租借DVD，每日应支付2元。");
						rent = (retDay - date[i])*2;
						System.out.println("应付租金："+rent);
						//数据更新
						state[i] = 0;//状态归为可借状态
						date[i] = 0;//记得把借出日期归零
					}
					else if(retName.equals(name[i]) && state[i] == 0)
					{
						retFlag = true;
						System.out.println("你所要借的DVD没有被借出，无法归还！");
						break;
					}
				}
			if(!retFlag)
			{
				System.out.println("这个DVD不是我们这边的，无法归还！");
				break;
			}
				break;
			//6.退出DVD
			case 6:
				flag = true;
				break;
			default:
				flag = true;
				break;
			}
			if(flag)
			{
				num = -1;
				break;
			}else{
				System.out.println("请输入0返回操作：");
				num = input.nextInt();
			}
		}while(num == 0);
		//这句话要放在do-while后面比较符合逻辑。
		System.out.println("谢谢使用DVD管理系统！");
	}

}
